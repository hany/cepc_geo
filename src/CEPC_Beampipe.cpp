#include "ACTFW/DD4hepDetector/DD4hepDetectorHelper.hpp"
#include "Acts/Plugins/DD4hep/ActsExtension.hpp"
#include "Acts/Plugins/DD4hep/IActsExtension.hpp"
#include "DD4hep/DetFactoryHelper.h"


using namespace std;
using namespace dd4hep;
using namespace dd4hep::detail;

static Ref_t create_element(Detector& description, xml_h e, Ref_t sens)  {
  xml_det_t  x_det = e;
  string     name  = x_det.nameStr();
  DetElement sdet(name,x_det.id());
  
  Acts::ActsExtension::Config volConfig;
  volConfig.isBeampipe           = true;
  Acts::ActsExtension* detvolume = new Acts::ActsExtension(volConfig);  
  sdet.addExtension<Acts::IActsExtension>(detvolume);
 
  dd4hep::xml::Dimension x_det_dim(x_det.dimensions());
  Tube  tube_shape(x_det_dim.rmin(), x_det_dim.rmax(), x_det_dim.z());
  Volume tube_vol(name,tube_shape,description.material(x_det_dim.attr<std::string>("material")));
  tube_vol.setVisAttributes(description,x_det_dim.visStr());
  // place Volume
  Volume mother_vol = description.pickMotherVolume(sdet);
  PlacedVolume placedTube = mother_vol.placeVolume(tube_vol);
  placedTube.addPhysVolID("tube", sdet.id());
  sdet.setPlacement(placedTube);
  return sdet;
  
  
}
DECLARE_DETELEMENT(CEPC_beampipe,create_element)
