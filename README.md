# CEPC_geo

CEPC geometry description for ACTS


--  version 2.0--


-- before using --

copy source file in ./src to /acts-fw/Detectors/DD4hepDetector/src
compile and reinstall the acts-fw


-- status --

Including Beampipe, Vertex, SIT, SET, FTD, ETD in the geometry description for DD4HEP and ACTS


updated to new barrel constructor


-- to do --

module size, readout and material
inner most layer of the vertex
